package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature{
	public Fahrenheit(float t){
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue();
		float output = (input - 32) / 9 * 5;
		Temperature out = new Celsius(output);
		return out;
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue();
		Temperature out = new Celsius(input);
		return out;
	}
	@Override
	public Temperature toKelvin() {
		float input = this.getValue();
		Temperature out = new Fahrenheit(input);
		Temperature output = out.toCelsius();
		input = (float) (output.getValue() + 273.15); 
		Temperature out2 = new Kelvin(input);
		return out2;
	}
}
