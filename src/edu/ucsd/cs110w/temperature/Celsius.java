package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature{
	public Celsius(float t){
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue();
		Temperature out = new Celsius(input);
		return out;
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue();
		float output = (input * 9 / 5) + 32;
		Temperature out = new Celsius(output);
		return out;
	}
	@Override
	public Temperature toKelvin() {
		float input = (float) (this.getValue() + 273.15);
		Temperature out = new Kelvin(input);
		return out;
	}
}
