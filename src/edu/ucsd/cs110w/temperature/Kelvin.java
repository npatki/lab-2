package edu.ucsd.cs110w.temperature;

public class Kelvin extends Temperature {
	public Kelvin(float t){
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		float input = this.getValue();
		input = (float) (input - 273.15);
		Temperature out = new Celsius(input);
		return out;
	}
	@Override
	public Temperature toFahrenheit() {
		float input = this.getValue();
		input = (float) (input - 273.15);
		float output = (input * 9 / 5) + 32;
		Temperature out = new Fahrenheit(output);
		return out;
	}
	
	public Temperature toKelvin() {
		float input = this.getValue();
		Temperature output = new Kelvin(input);
		return output;
	}
}
